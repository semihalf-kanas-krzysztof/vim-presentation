# vim-presentation



# Couple tips & tricks for vim users

## install.sh

Host your .vimrc  or your .rc files in git repository (best private)
Clone and install them on all machines, but most imprtantly *version* them.

Easy installation on every machine
```
git clone git@your-favorite-git-hosting.com:user/dotfiles.git dotfiles
cd dotfiles
install.sh
```


## keyboard

### Capslock is useless - remap to ctrl

#### Linux + X11
/usr/bin/setxkbmap  -option 'ctrl:nocaps'

#### ChromeOs
keyboard/chromeos-keybboard.jpg

#### Windows
Varies with versions
https://gist.github.com/krists/8898275
Autohotkey
https://github.com/fabiofortkamp/homectrl

#### MacOs
?

###  ctrl-z & zsh
Use ctrl-z back and forth
fancy-ctrl-z.zsh


###  zsh & bindkey -e || bindkey -v

To use vim keybindings use:
``` bindkey -v ```

but if you really miss EMACS keybindings
try using
```
    bindkey -v
    bindkey "^P"   up-line-or-history
    bindkey "^N"   down-line-or-history
    bindkey "^A"   beginning-of-line
    bindkey "^K"   kill-line
    bindkey "^R"   history-incremental-search-backward
    # Alt .
    bindkey '^[.'  insert-last-word
    # remove anoying vi-up-line-or-history
    # because I acidenty type - when trying to get 0
    bindkey -rM vicmd '\-'
    # this is how you rebind vicmd to sth else
    # bindkey -M vicmd '\-' beep
    # Remove ctrl-D ^D delete-char-or-list, delete last character
    # and provide compleation for remaining of cmdline
    bindkey -rM vicmd '^D'
    bindkey -r '^D'

```


##  key mapping & mapleader

### qucikfix copen
```
let g:quckfix_size='5'
nnoremap <silent> <leader>q :call ToggleList("Quickfix List", 'c',  g:quckfix_size, 'no')<CR>

function! GetBufferList()
  redir =>buflist
  silent! ls
  redir END
  return buflist
endfunction

function! ToggleList(bufname, pfx, num, switchTo)
  let buflist = GetBufferList()
  for bufnum in map(filter(split(buflist, '\n'), 'v:val =~ "'.a:bufname.'"'), 'str2nr(matchstr(v:val, "\\d\\+"))')
    if bufwinnr(bufnum) != -1
        exec(a:pfx.'close')
        return
    endif
  endfor
  if a:pfx == 'l' && len(getloclist(0)) == 0
    echohl ErrorMsg
    echo "Location List is Empty."
    return
  endif
  let winnr = winnr()
  exec('botright '.a:pfx.'open'.' '.a:num)
  if winnr() != winnr
    if a:switchTo == 'yes'
      wincmd p
    endif
  endif
endfunction
```

### qucikfix & checkpatch
```
nnoremap <silent> <leader>p :call DoCheckPatchFile('%')<cr>
let g:checkpatch_path='scripts/checkpatch.pl'
let g:checkpatch_opts=' --terse --file '

function! DoCheckPatchFile(file)
	let l:mkprg=&makeprg
	echo l:mkprg
	let &makeprg=g:checkpatch_path . ' ' . g:checkpatch_opts . ' %'
	:make
	let &makeprg=l:mkprg
endfunction
```

Map Ctrl-L to file name  and ctrl-s to search register
```
" insert search term but without two first and two last characters aka " </word/>
cnoremap <c-s> <c-r>=substitute(@/, '^\\<\(.*\)\\>$','\1', '')<cr>
inoremap <c-s> <c-r>=substitute(@/, '^\\<\(.*\)\\>$','\1', '')<cr>
" insert current $(dirname $cur_file)
cnoremap <c-l> <C-r>=expand('%:h')<cr>
cnoremap <c-o> <C-r>=expand('%')<cr>
```

##  Commands & functions

Annoying sidebars for copy & paste from console tmux
Get rid of them using ToggleSidebars

```
function! ToggleSidebars()
  if &relativenumber || &number
    set nonumber
    set nornu
  else
    set rnu
  endif
endfunction

command! -bar -nargs=0  ToggleSidebars call ToggleSidebars()
```

Invisible spaces vs tabs plauges your python scripts
(or make checkpatch complains) see them if you wish
```
function! ToggleListChars()
	if &list
		set nolist
	else
		set list
		set listchars=tab:.-
	endif
endfunction

command! -bar -nargs=0  ToggleListChars call ToggleListChars()
```






# Links
https://gist.github.com/nifl/1178878
https://stackoverflow.com/questions/1218390/what-is-your-most-productive-shortcut-with-vim
https://youtu.be/wlR5gYd6um0

https://learnvimscriptthehardway.stevelosh.com/

Everything from Tim Pope
https://tbaggery.com/
https://blog.carbonfive.com/vim-text-objects-the-definitive-guide/

vim Keyboard
https://dave.cheney.net/2017/08/21/the-here-is-key
